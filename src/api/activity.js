import axios from 'axios'

const BASE_URL = 'https://www.boredapi.com/api/'
const ACTIVITY = 'activity'

export const apiGetActivity = () => {
	return axios.get(BASE_URL + ACTIVITY).then((response) => response.data)
}

export const apiGetCustomActivity = (query) => {
	return axios.get(BASE_URL + ACTIVITY, { params: { ...query } }).then((response) => response.data)
}
