import React, { useState } from 'react';
import Activity from './components/Activity/Activity'
import { apiGetCustomActivity, apiGetActivity } from './api/activity'

function App() {
	const [participants, setParticipants] = useState('');
	const [type, setType] = useState('');
	const [minPrice, setMinPrice] = useState(0);
	const [maxPrice, setMaxPrice] = useState(1);
	const [loading, setLoading] = useState(false)
	const [results, setResults] = useState({})

	const getInputData = (e) => {
		e.preventDefault();
		setLoading(true)

		// if (maxPrice === 1 || minPrice === 0) {
		// 	setMinPrice('');
		// 	setMaxPrice('');
		// } else {
		// 	setMinPrice(0);
		// 	setMaxPrice(1);
		// }

		const newRequest = {
			participants,
			type,
			minprice: minPrice,
			maxprice: maxPrice
		};

		setParticipants('');
		setType('');
		setMinPrice(0);
		setMaxPrice(1);

		for (const key in newRequest) {
			if (newRequest[key] === '') {
				delete newRequest[key];
			}
		}
		apiGetCustomActivity(newRequest).then((activity) => {
			setResults(activity)
			setLoading(false)
		}).catch((error) => console.log(error)).finally(() => {
			setLoading(false)
		})
	};

	const getActivity = () => {
		setLoading(true)
		apiGetActivity().then((activity) => {
			setResults(activity)
			setLoading(false)
		}).catch((error) => console.log(error)).finally(() => {
			setLoading(false)
		})
	}

	return (
		<div className='App'>
			<div className="container">
				<h1>Boredom strikes again...</h1>
				<form>
					<fieldset>
						<div className="mb-3">
							<label htmlFor='participants' className='form-label'>Number of participants</label>
							<input type='number' name='' id='participants' placeholder='How many people?' className='form-control'
								value={participants} onChange={e => setParticipants(e.target.value)}
							/>
						</div>
						<div className="mb-3">
							<label htmlFor='type' className='form-label'>Select type of activity</label>
							<select id="type" name="type" className="form-select" value={type} onChange={e => setType(e.target.value)} >
								<option value="" disabled>Choose activity</option>
								<option value="education">education</option>
								<option value="recreational">recreational</option>
								<option value="social">social</option>
								<option value="diy">diy</option>
								<option value="charity">charity</option>
								<option value="cooking">cooking</option>
								<option value="relaxation">relaxation</option>
								<option value="music">music</option>
								<option value="busywork”">busywork</option>
							</select>
						</div>

						<div className="mb-3">
							<label htmlFor="customRange1" className="form-label">Minimum price</label>{" "}<output>{minPrice}</output>
							<input type="range" className="form-range" min="0" max="1" step="0.1" id="customRange1" value={minPrice} onChange={e => setMinPrice(e.target.value)}></input>

						</div>

						<div className="mb-3">
							<label htmlFor="customRange2" className="form-label">Maximum price</label>{" "}<output>{maxPrice}</output>
							<input type="range" className="form-range" min="0" max="1" step="0.1" id="customRange2" value={maxPrice} onChange={e => setMaxPrice(e.target.value)}></input>

						</div>
						<button type="button" className='btn btn-primary' onClick={getInputData}>Get personal activity</button>
						<button type="button" className="btn btn-secondary" onClick={getActivity}>Get random activity</button>
					</fieldset>
				</form>
				{loading === true
					? <span>Loading....</span>
					: <Activity results={results} />}
			</div>
		</div>
	)
}

export default App

